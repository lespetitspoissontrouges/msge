
<div id="accroche">Plans de la M2L</div>

<h1>Implantation des quatre bâtiments</h1>
<img src="images/4batiments.png">

<h1>Implantation des locaux en rez-de-chaussée des quatre bâtiments</h1>
<img src="images/locaux.png">

<h1>Plan standard d'étage : l'exemple du deuxième étage du bâtiment A</h1>
<img src="images/etage.png">

<h1>Schémas d'implantation des baies de brassage</h1>

<p>Au Rez-de-chaussée (du bâtiment B)</p>
<img src="images/batimentB.png">

<p>Dans un étage du bâtiment A</p>
<img src="images/etageA2.png">
