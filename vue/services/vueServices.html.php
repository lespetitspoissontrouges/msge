<h1>Services</h1>
<br>
<ul>
    <li>Accès Internet</li>
    <li>Accès Wifi</li>
    <li>Téléphonie</li>
    <li>Affranchissement</li>
    <li>Impressions en volume et en qualité imprimerie</li>
    <li>Serveur FTP documentaire</li>
    <li>Système de réservation des salles</li>
    <li>Information sur le digicode du jour et la clé Wifi</li>
    <li>Système de gestion des configurations</li>
    <li>Intégration des postes informatiques des ligues</li>
    <li>Intégration d'imprimantes</li>
    <li>Service d'établissement de bulletins de salaire</li>
    <li>Formations</li>
</ul>
