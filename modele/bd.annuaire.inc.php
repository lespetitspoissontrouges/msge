<?php

include_once "bd.inc.php";


function getLigues() : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from ligue");
        $req->execute();

        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) {
            $resultat[] = $ligne;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}


function getLigueById(int $idLigue) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from ligue where idligue = :idLigue");
        $req->bindValue(':idLigue', $idLigue , PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getLigueByDiscipline(string $nomDisc) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from ligue inner join discipline on ligue.idligue = discipline.iddiscipline where lower(discipline.libelle) like :nomDisc");
        $req->bindValue(':nomDisc', strtolower("%$nomDisc%") , PDO::PARAM_STR);

        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function modifierLigue(int $idLigue, string $adresse) : bool {
    $resultat = -1;
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("update ligue set adresse=:adresse where idligue=:idLigue");
        $req->bindValue(':adresse', $adresse, PDO::PARAM_STR);
        $req->bindValue(':idLigue', $idLigue, PDO::PARAM_INT);
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getComites() : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from comitedepartemental");
        $req->execute();

        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) {
            $resultat[] = $ligne;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getComiteById(int $idComite) : array {
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from comitedepartemental where idcomite = :idComite");
        $req->bindValue(':idComite', $idComite , PDO::PARAM_INT);

        $req->execute();

        $resultat = $req->fetch(PDO::FETCH_ASSOC);

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getComitesByDiscipline(string $nomDisc) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select comitedepartemental.nom from comitedepartemental inner join ligue on comitedepartemental.idligue = ligue.idligue inner join discipline on ligue.idligue = discipline.iddiscipline where lower(discipline.libelle) like :nomDisc");
        $req->bindValue(':nomDisc', strtolower("%$nomDisc%") , PDO::PARAM_STR);

        $req->execute();

        $ligne = $req->fetch(PDO::FETCH_ASSOC);
        while ($ligne) {
            $resultat[] = $ligne;
            $ligne = $req->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function modifierComite(int $idComite, string $adresse) : bool {
    $resultat = -1;
    try {
        $cnx = connexionPDO();

        $req = $cnx->prepare("update ligue set adresse=:adresse where idcomite=:idComite");
        $req->bindValue(':adresse', $adresse, PDO::PARAM_STR);
        $req->bindValue(':idComite', $idComite, PDO::PARAM_INT);
        $resultat = $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getListeMembres(int $idLigue) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select nom, mail from membres where idligue = :idLigue");
        $req->bindValue(':idLigue', $idLigue , PDO::PARAM_INT);

        $req->execute();

        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) {
            $resultat[] = $ligne;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

function getListeMembresComite(int $idComite) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select nom, mail from membrescomite where idcomite = :idComite");
        $req->bindValue(':idComite', $idComite , PDO::PARAM_INT);

        $req->execute();

        while ($ligne = $req->fetch(PDO::FETCH_ASSOC)) {
            $resultat[] = $ligne;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getLigues() : \n";
    print_r(getLigues());

    echo "getLigueById(idLigue) : \n";
    print_r(getLigueById(1));

    echo "getLigueByDiscipline(discipline) : \n";
    print_r(getLigueByDiscipline('Judo'));

    echo "getComitesByDiscipline(discipline) : \n";
    print_r(getComitesByDiscipline('Handball'));

    echo "modifierLigue(idLigue) : \n";
    print_r(modifierLigue(1,'Nouvelle adresse'));

    echo "getComite() : \n";
    print_r(getComites());

    echo "getComiteById(idComite) : \n";
    print_r(getComiteById(1));

}
?>